require "grape"
require "grape-entity"

require_relative "lib/sms_domain"

module Gugud
  class Api < Grape::API
    version "v1", using: :path, vendor: "intermaker.cn"
    format :json
    prefix :api

    # -----------------------------------------------------
    # 出错信息处理
    # -----------------------------------------------------
    default_error_status 400
    rescue_from :all do |e|
      error!({reason: e.message, with: ErrorEntity})
    end


    # -----------------------------------------------------
    # Restful API 定义
    # -----------------------------------------------------
    resource :sms do
      desc "发送文本消息"
      params do
        requires :apikey, type: String, desc: "云片服务器许可验证"
        requires :mobile, type: String, desc: "手机号码", regexp: /^1[0-9]{10}$/
        requires :text, type: String, desc: "发送内容"
      end
      post "/yunpian" do
        SMS::YunPian.new(params[:apikey]).send(params[:mobile], params[:text])
        # {result: :ok}
      end
    end
  end
end
