require "httparty"

## --------------------------------------------------------
## 短信业务领域
## --------------------------------------------------------
module SMS
  class YunPian
    include HTTParty
    base_uri "http://yunpian.com"

    def initialize apikey
      @apikey = apikey
    end

    def send mobile, text
      self.class.post("/v1/sms/send.json",
        headers: {"Content-Type" => "application/x-www-form-urlencoded"},
        query: {apikey: @apikey, mobile: mobile, text: text})
    end
  end
end
